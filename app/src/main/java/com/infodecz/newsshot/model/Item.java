package com.infodecz.newsshot.model;

import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("tags")
    private String tags;
    @SerializedName("uid")
    private String uid;
    @SerializedName("icon")
    private String icon;
    @SerializedName("sourcetitle")
    private String sourcetitle;
    @SerializedName("updatetime")
    private String updatetime;
    @SerializedName("link")
    private String link;
    @SerializedName("unread")
    private String unread;
    @SerializedName("id")
    private String id;
    @SerializedName("content")
    private String content;
    @SerializedName("author")
    private String author;
    @SerializedName("title")
    private String title;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("source")
    private String source;
    @SerializedName("starred")
    private String starred;
    @SerializedName("datetime")
    private String datetime;

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSourcetitle() {
        return sourcetitle;
    }

    public void setSourcetitle(String sourcetitle) {
        this.sourcetitle = sourcetitle;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStarred() {
        return starred;
    }

    public void setStarred(String starred) {
        this.starred = starred;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        return "ClassPojo [tags = " + tags + ", uid = " + uid + ", icon = " + icon + ", sourcetitle = " + sourcetitle + ", updatetime = " + updatetime + ", link = " + link + ", unread = " + unread + ", id = " + id + ", content = " + content + ", author = " + author + ", title = " + title + ", thumbnail = " + thumbnail + ", source = " + source + ", starred = " + starred + ", datetime = " + datetime + "]";
    }
}
