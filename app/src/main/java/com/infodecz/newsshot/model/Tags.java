package com.infodecz.newsshot.model;

import com.google.gson.annotations.SerializedName;

public class Tags {
    @SerializedName("color")
    private String color;
    @SerializedName("tag")
    private String tag;
    @SerializedName("unread")
    private String unread;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    @Override
    public String toString() {
        return "ClassPojo [color = " + color + ", tag = " + tag + ", unread = " + unread + "]";
    }
}
