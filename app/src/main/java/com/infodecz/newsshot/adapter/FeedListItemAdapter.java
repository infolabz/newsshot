package com.infodecz.newsshot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.infodecz.newsshot.R;
import com.infodecz.newsshot.model.Item;

import java.util.ArrayList;

public class FeedListItemAdapter extends RecyclerView.Adapter<FeedListItemAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "FeedListItemAdapter";
    private Context mContext = null;
    private ArrayList<Item> mFeedList;
    private FeedlistItemListener mFeedItemClickListener;

    public FeedListItemAdapter(Context context, ArrayList<Item> feedItemsList, FeedlistItemListener listener) {
        mContext = context;
        mFeedList = feedItemsList;
        mFeedItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.feed_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Item feedItem = mFeedList.get(position);
        Log.d(TAG, "Feed size :: " + feedItem.getTitle());
        holder.textTitle.setText(feedItem.getTitle());
        holder.textRating.setText("1");
        holder.textYear.setText("2013");
        holder.view.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return mFeedList.size();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_layout:
                mFeedItemClickListener.onItemClick();
        }
    }

    public interface FeedlistItemListener {
        void onItemClick();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textTitle;
        private final TextView textRating;
        private final TextView textYear;
        private final View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.main_layout);
            textTitle = itemView.findViewById(R.id.main_title);
            textRating = itemView.findViewById(R.id.main_rating);
            textYear = itemView.findViewById(R.id.main_year);
        }
    }
}
