
package com.infodecz.newsshot;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infodecz.newsshot.adapter.FeedListItemAdapter;
import com.infodecz.newsshot.model.Item;
import com.infodecz.newsshot.service.IResult;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IResult, FeedListItemAdapter.FeedlistItemListener {

    private static final String TAG = "MainActivity";
    private RecyclerView mFeedListView;
    private LinearLayoutManager mLinearLayoutManager;
    private DividerItemDecoration mDividerItemDecoration;
    private ArrayList<Item> mFeedList;
    private RecyclerView.Adapter mFeedAdapter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        mContext = this;
        mFeedListView = findViewById(R.id.main_feed_list);
        mFeedList = new ArrayList<>();
        mFeedAdapter = new FeedListItemAdapter(mContext, mFeedList, this);

        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mDividerItemDecoration = new DividerItemDecoration(mContext, mLinearLayoutManager.getOrientation());

//        mFeedListView.addItemDecoration(mDividerItemDecoration);
//        mFeedListView.setItemAnimator(new DefaultItemAnimator());
        mFeedListView.setLayoutManager(mLinearLayoutManager);
        mFeedListView.setAdapter(mFeedAdapter);
        getData();

//        DataService service = new DataService(this, this);

//        service.getDataCall("items", "items");
//        service.getDummy("items");
    }

    @Override
    public void onSuccess(String requestType, JSONArray response) {
//        switch (requestType){
//            case "items":
        Gson gson = new Gson();
        Type collectionType = new TypeToken<List<Item>>() {
        }.getType();
        mFeedList = gson.fromJson(response.toString(), collectionType);
        Log.d(TAG, "Feed size :: " + mFeedList.size());
        mFeedAdapter.notifyDataSetChanged();
        Log.d(TAG, response.toString());
//        }
    }

    @Override
    public void onFailure(String requestType, VolleyError error) {
        Log.d(TAG, error.toString());
    }

    public void getData() {
        for (int i = 1; i < 11; i++) {
            Item item = new Item();
            item.setTitle("Title " + i);
            item.setContent("Content " + i);
            mFeedList.add(item);
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick() {

    }
}
