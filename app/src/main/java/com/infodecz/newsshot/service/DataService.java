package com.infodecz.newsshot.service;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DataService {
    private static final String HOST_URL = "http://infodecz.com/selfoss_rss/";
    IResult mResultCallBack = null;
    Context mContext = null;

    public DataService(Context context, IResult resultCallBack) {

        mContext = context;
        mResultCallBack = resultCallBack;
    }

    public void getDataCall(final String requestType, String url) {

        url = HOST_URL + url;

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JsonArrayRequest request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (mResultCallBack != null) {
                    mResultCallBack.onSuccess(requestType, response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallBack != null) {
                    mResultCallBack.onFailure(requestType, error);
                }
            }
        });
    }

    public void getDummy(String requestType) {
// Reading json file from assets folder
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(mContext.getAssets().open(
                    "items.json")));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close(); // stop reading
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String myjsonstring = sb.toString();
        JSONArray jsonArray = null;
        // Try to parse JSON
        try {
            // Creating JSONArray from JSONObject
            jsonArray = new JSONArray(myjsonstring);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mResultCallBack.onSuccess(requestType, jsonArray);
    }
}
