package com.infodecz.newsshot.service;

import com.android.volley.VolleyError;

import org.json.JSONArray;

public interface IResult {
    void onSuccess(String requestType, JSONArray response);

    void onFailure(String requestType, VolleyError error);
}
